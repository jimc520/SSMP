import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import top.jimc.ssmp.mapper.UserMapper;
import top.jimc.ssmp.po.User;

/**
 * @author Jimc.
 * @since 2018/10/17.
 */
public class SqlTest extends BaseJunit4Test {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void selectPageTest() {
        IPage<User> page = userMapper.selectPage(new Page<>(2, 10), null);
        System.out.println(JSON.toJSONString(page, true));
    }
}
