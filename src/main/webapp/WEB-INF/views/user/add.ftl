<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加用户信息</title>
</head>
<body>
<fieldset>
    <legend>添加用户</legend>
    <form action="" id="fom">
        <div><label>用户名称：<input type="text" name="username"></label></div>
        <div><label>用户密码：<input type="text" name="password"></label></div>
        <div><label>用户邮箱：<input type="text" name="email"></label></div>
        <div><input id="addBtn" type="button" value="提交"></div>
    </form>
</fieldset>
<script type="text/javascript" src="${ctxPath}/resources/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="${ctxPath}/resources/js/jquery.form.js"></script>
<script type="text/javascript">
    $(function () {
        $("#addBtn").on('click', function () {
            /*let d = {};
            let t = $('form').serializeArray();
            $.each(t, function () {
                d[this.name] = this.value;
            });*/
            /*$.ajax({
                url: "${ctxPath}/user/add",
                data: d,
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    // alert(data);
                },
                error: function () {
                    // alert(data);
                }
            });*/
            $('#fom').ajaxSubmit({
                url: '${ctxPath}/user/add',
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        location.href = '${ctxPath}/user/listPage';
                        alert(data.msg);
                    } else {
                        alert(data.msg);
                    }
                },
                error: function (data) {
                    alert('error')
                }
            });
        });
    })

</script>
</body>
</html>