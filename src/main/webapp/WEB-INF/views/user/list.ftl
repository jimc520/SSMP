<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户信息列表展示</title>
</head>
<body>
<fieldset>
    <legend>用户列表</legend>
    <div>
        <a href="${ctxPath}/user/addPage">添加</a>
    </div>
    <div>
        <table border="1">
            <tr>
                <th>用户名</th>
                <th>密码</th>
                <th>邮箱</th>
                <th>操作</th>
            </tr>
            <#if page??>
                <#if page.records??>
                    <#list page.records as record>
                    <tr>
                        <td>${record.username}</td>
                        <td>${record.password}</td>
                        <td>${record.email}</td>
                        <td><a href="${ctxPath}/user/updatePage?userId=${record.id}">修改</a>|<a href="javascript:del(${record.id});">删除</a></td>
                    </tr>
                    </#list>
                </#if>
                <tr>
                    <td colspan="4" style="text-align: right">
                        <span>共${totalPage}页 </span>
                        <a href="javascript:page(1);">首页 </a>
                        <a href="javascript:page(${page.current}, 'up');">上一页 </a>
                        <a href="javascript:page(${page.current}, 'dn', ${totalPage});">下一页 </a>
                        <a href="javascript:page(${totalPage});">尾页</a>
                    </td>

                </tr>
            </#if>
        </table>
    </div>
</fieldset>
<script type="text/javascript" src="${ctxPath}/resources/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">

    $(function () {
        <#--console.log(JSON.stringify(${page}))-->
    })
    function del(id) {
        var conf = confirm("确认删除？");
        if (conf){
            $.ajax({
                url: '${ctxPath}/user/delete',
                data: {
                    userId: id
                },
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        location.href = '${ctxPath}/user/listPage';
                        alert(data.msg);
                    } else {
                        alert(data.msg);
                    }
                },
                error: function () {
                    // alert(data);
                }
            })
        }
    }

    function page(pageNo, mk, totalPage) {
        if (pageNo == null){
            pageNo = 1;
        }
        if (mk === 'up'){
            pageNo = pageNo -1 > 0 ? pageNo -1 : 1;
        }else if(mk === 'dn'){
            pageNo = pageNo + 1 <= totalPage ? pageNo + 1 : totalPage;
        }
        location.href = '${ctxPath}/user/listPage?current='+pageNo+'&size=5';
    }
</script>
</body>
</html>