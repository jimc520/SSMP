package top.jimc.ssmp.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import top.jimc.ssmp.common.base.BaseController;
import top.jimc.ssmp.po.User;
import top.jimc.ssmp.service.UserService;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jimc.
 * @since 2018-10-17
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @RequestMapping("/listPage")
    public ModelAndView listPage(Integer current, Integer size) {
        if (ObjectUtil.isNull(current) || ObjectUtil.isNull(size)){
            current = 1;
            size = 5;
        }
        ModelAndView mv = new ModelAndView("user/list");
//        mv.addObject("users", userService.list(null));
        IPage<User> page = userService.page(new Page<>(current, size),null);
        mv.addObject("page", page);
        mv.addObject("totalPage", page.getPages());
        return mv;
    }

    @RequestMapping("/addPage")
    public ModelAndView addPage() {
        return new ModelAndView("user/add");
    }

    @RequestMapping("/add")
    @ResponseBody
    public Map<String, Object> add(User user) {
        Map<String, Object> result = new HashMap<>();
        try {
            userService.save(user);
            result.put("success", true);
            result.put("msg", "添加成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("success", false);
            result.put("msg", "添加失败");
        }
        return result;
    }

    @RequestMapping("updatePage")
    public ModelAndView updatePage(Integer userId) {
        ModelAndView mv = new ModelAndView("user/update");
        mv.addObject("user", userService.getById(userId));
        return mv;
    }

    @RequestMapping("update")
    @ResponseBody
    public Map<String, Object> update(User user) {
        Map<String, Object> result = new HashMap<>();
        try {
            userService.updateById(user);
            result.put("success", true);
            result.put("msg", "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("success", false);
            result.put("msg", "修改失败");
        }
        return result;
    }

    @RequestMapping("delete")
    @ResponseBody
    public Map<String, Object> delete(Integer userId) {
        Map<String, Object> result = new HashMap<>();
        try {
            userService.removeById(userId);
            result.put("success", true);
            result.put("msg", "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("success", false);
            result.put("msg", "删除失败");
        }
        return result;
    }
}
