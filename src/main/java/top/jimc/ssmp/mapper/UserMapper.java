package top.jimc.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.jimc.ssmp.po.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Jimc.
 * @since 2018-10-17
 */
public interface UserMapper extends BaseMapper<User> {

}
