package top.jimc.ssmp.common.generator;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.setting.dialect.Props;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * 代码生成器
 *
 * @author Jimc.
 * @since 2018/10/17.
 */
public class CodeGenerator {

    public static void generate(String... tableNames) {
        // 读取数据库属性配置文件
        Props props = new Props("jdbc.properties", CharsetUtil.UTF_8);

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setActiveRecord(true)// 是否开启 ActiveRecord 模式
                .setIdType(IdType.UUID)// 指定生成的主键的ID类型
                .setAuthor("Jimc.")
                .setBaseResultMap(true)// 是否开启 BaseResultMap
                .setBaseColumnList(true)// 是否开启 baseColumnList
                .setFileOverride(true)// 是否覆盖已有文件
                .setEnableCache(false)// 是否在xml中添加二级缓存配置
                .setOpen(false)// 是否打开输出目录
                .setMapperName("%sMapper")
                .setXmlName("%sMapper")
                .setServiceName("%sService")
                .setServiceImplName("%sServiceImpl")
                .setOutputDir("generatorCode");

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL)
                .setDriverName(props.getStr("jdbc.driver_class"))
                .setUrl(props.getStr("jdbc.url"))
                .setUsername(props.getStr("jdbc.username"))
                .setPassword(props.getStr("jdbc.password"))
                .setTypeConvert(new MySqlTypeConvert() {
                    // 自定义数据库表字段类型转换【可选】
                    public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                        return super.processTypeConvert(globalConfig, fieldType);
                    }
                });

        // 数据库表策略配置
        StrategyConfig sc = new StrategyConfig();
        sc.setNaming(NamingStrategy.underline_to_camel)// 表名生成策略
                .setColumnNaming(NamingStrategy.underline_to_camel)// 字段名生成策略，未指定按照 naming 执行
                .entityTableFieldAnnotationEnable(true)// 是否生成实体时，生成字段注解
                .setEntityLombokModel(false)//【实体】是否为lombok模型
                .setEntityBuilderModel(false)//【实体】是否为构建者模型
                .setControllerMappingHyphenStyle(true)
                .setSuperControllerClass("top.jimc.ssmp.common.base.BaseController");
        if (ArrayUtil.isEmpty(tableNames)) {
            System.err.println("输入表名");
            return;
        }
        sc.setInclude(tableNames);


        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName(null)//所属模块
                .setParent("top.jimc.ssmp")// 自定义包路径
                .setController("controller")// 控制器包名，默认 web
                .setEntity("po")
                .setMapper("mapper")
                .setXml("xml");

        // 代码生成器
        new AutoGenerator()
                .setTemplateEngine(new FreemarkerTemplateEngine())
                .setGlobalConfig(gc)
                .setDataSource(dsc)
                .setStrategy(sc)
                .setPackageInfo(pc)
                .execute();
    }

    public static void main(String[] args) {
        generate("user");
    }


}
