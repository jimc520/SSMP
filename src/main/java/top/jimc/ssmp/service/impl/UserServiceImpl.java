package top.jimc.ssmp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.jimc.ssmp.mapper.UserMapper;
import top.jimc.ssmp.po.User;
import top.jimc.ssmp.service.UserService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jimc.
 * @since 2018-10-17
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
