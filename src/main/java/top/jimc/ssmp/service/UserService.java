package top.jimc.ssmp.service;

import top.jimc.ssmp.po.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jimc.
 * @since 2018-10-17
 */
public interface UserService extends IService<User> {

}
